function findTheYears(inventory) {

    if (!Array.isArray(inventory)) {
        throw new Error('The inventory must be an array');
    }

    let years = [];

    for (let i = 0; i < inventory.length; i++) {

        if (!inventory[i].car_year) {
            throw new Error('Each item in the inventory must have a `car_year` property');
        }

        years.push(inventory[i].car_year);
    }

    return years;
}

module.exports = findTheYears;