const findTheYears = require('./problem4');

function countOfCars(inventory) {

    if (!Array.isArray(inventory)) {
        throw new Error('The inventory must be an array');
    }

    let years;
    try {
        years = findTheYears(inventory);
    }
    catch (error) {
        throw new Error('Error in findTheYears function: ' + error.message);
    }

    if (!Array.isArray(years)) {
        throw new Error('The findTheYears function must return an array');
    }

    let count = 0;
    for (let i = 0; i < years.length; i++) {
        if (years[i] < 2000) {
            count++;
        }
    }

    return count;
}

module.exports = countOfCars;