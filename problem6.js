function getBMWandAudiCars(inventory) {

    if (!Array.isArray(inventory)) {
        throw new Error('The inventory must be an array');
    }

    let BMWandAudi = [];
    for (let i = 0; i < inventory.length; i++) {

        if (!inventory[i].car_make) {
            throw new Error('Each item in the inventory must have a `car_make` property');
        }

        if (inventory[i].car_make === 'BMW' || inventory[i].car_make === 'Audi') {
            BMWandAudi.push(inventory[i]);
        }
    }

    return BMWandAudi;
}

module.exports = getBMWandAudiCars;