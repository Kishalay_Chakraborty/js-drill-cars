function getLastCar(inventory) {

    if (!Array.isArray(inventory)) {
        throw new Error('The inventory must be an array');
    }

    if (inventory.length === 0) {
        return null;
    }
    
    return inventory[inventory.length - 1];
}

module.exports = getLastCar;
