function findCar33(inventory, id) {
    
    if (!Array.isArray(inventory)) {
        throw new Error('The inventory must be an array');
    }

    if (typeof id !== 'number') {
        throw new Error('The ID must be a number');
    }

    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].id === id) {
            return inventory[i];
        }
    }

    return null;
}

module.exports = findCar33;