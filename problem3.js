function sortCarModels(inventory) {

    if (!Array.isArray(inventory)) {
        throw new Error('The inventory must be an array');
    }

    let carModels = [];
    for (let i = 0; i < inventory.length; i++) {
        if (!inventory[i].car_model) {
            throw new Error('Each item in the inventory must have a `car_model` property');
        }
        carModels.push(inventory[i].car_model);
    }
    carModels.sort();
    return carModels;
}

module.exports = sortCarModels;
