const findCar33 = require('../problem1');
const inventory = require('./data.js');

const car = findCar33(inventory, 33);

console.log(`Car 33 is a ${car.car_year} ${car.car_make} ${car.car_model}`);
