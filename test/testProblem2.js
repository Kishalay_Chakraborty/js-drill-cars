const getLastCar = require('../problem2');
const inventory = require('./data.js');


const lastCar = getLastCar(inventory);
console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
